% Load the kaohandt class (with the default options)
\documentclass[
  fontsize=10pt, % Base font size
  twoside=false, % If true, use different layouts for even and odd pages (in particular, if twoside=true, the margin column will be always on the outside)
  %secnumdepth=2, % How deep to number headings. Defaults to 2 (subsections)
  %abstract=true, % Uncomment to print the title of the abstract
]{kaohandt}

% Choose the language
\usepackage[english]{babel} % Load characters and hyphenation
\usepackage[english=british]{csquotes}	% English quotes

\usepackage{booktabs}
\usepackage[activate={true,nocompatibility},final,tracking=true,kerning=true,spacing=true,factor=1100,stretch=10,shrink=10]{microtype}

% Load mathematical packages for theorems and related environments.
\usepackage{kaotheorems}

% Load the bibliography package
\usepackage{kaobiblio}
\addbibresource{main.bib} % Bibliography file

\usepackage{kaorefs}

%----------------------------------------------------------------------------------------

\begin{document}

\title[Project Proposal]{Project Proposal - Analyzing failures in wind turbine bearings}
\author[VPK]{Vishakh Pradeep Kumar \thanks{vp2039@hw.ac.uk; H00335300}}
\date{\today}
\maketitle

\section{Introduction}

% Obligatory paragraph about wind power and why it is important
The share of wind energy \sidenote{ The total capacity of onshore wind farms is expected to triple by 2030 and quadruple by 2050 \cite{irena2019}, with new developments focusing on offshore wind farms due to higher wind speeds and land availability.} in electrical power generation is increasing due to the need to fulfil climate goals and to contain the rise of global temperatures \cite{AKDAG20091761}. Additionally, wind energy is a domestic source of energy that improves the security issues associated with being dependent on fossil fuel imports, with the decreasing cost making it a competitive alternative \cite{jmse10111661}.





\begin{table}[ht]
\begin{tabular}{ c c c c }
  \toprule
  Power & Failures/Turb./Year & Downtime/Failure \\
  \midrule
Less than 1MW  & 0.46 & 151.46 h \\
More than 1MW & 0.52 & 112.67 h \\
  \bottomrule
\end{tabular}
\caption[Wind Turbine Failure Statistics]{Wind Turbine Failure Statistics \cite{WISER201946}}
\labtab{wind_turbine_downtime}
\end{table}

However, there exists an urgent need to tackle turbine gearbox failures and downtimes as it creates increases in the capital and operating costs. Despite being specially designed to withstand the harsh operating conditions of multi-megawatt turbines for 20 years, wind turbine gearboxes \sidenote{Wind turbine gearboxes are used to convert the rotational speed of the rotor into the speed required for electricity production; a commonly used ratio is 100:1 \cite{JANTARA202069}.} are expected to have only a lifetime of 7-10 years \cite{MichelleGearboxBlog}. Gearbox-related expenditures contribute almost half of the total operational expenditures, which can account for 25\%-35\% of land-based and offshore wind LCOE \cite{riva2019iea}. \sidenote{OpEx costs about 44 USD/kW-yr or 12 USD/MWh \cite{riva2019iea}. Replacing a turbine gearbox requires labor \& crane rentals, in addition to capital expense and revenue loss; onshore gearbox failures can cost 250,000-300,000 USD per failure event” \cite{STLEBlog}.} Thus, gearboxes are a prime area of research, as improvements in gearboxes can greatly improve reliability and profitability, and the proposed report will focus on ways to tackle gearbox failure.

\section{Bearing failures and damage mechanisms}

Understanding gearbox failures can be complicated due to complex wind loading and critical short-term events such as start-ups, shut-downs, and emergency stops. The NREL Gearbox Reliability Database (GRD) shows that gearbox failures can be primarily attributed to bearings (76\%) with the damage mechanisms being micropitting, scuffing, and white etching cracks. The top failure modes for bearings are cracking due to localized stress, abrasion or gouges due to aspertites and/or contaminents, and adhesion due to welding \& tearing between surfaces.

\begin{figure*}
  \includegraphics{white_etching_location}
  \caption[Location of white etch cracking]{Example of axial crack \cite{LOPEZURUNUELA2021106091}}
  \labfig{white_etching_location}
\end{figure*}

Bearing design life is assessed through International Organization for Standardization standards 76 and 281 \sidenote{\href{https://www.iso.org/standard/38101.html}{ISO 76} specifies static load ratings for roller bearings manufactured from hardened bearing steel and \href{https://www.iso.org/standard/38102.html}{ISO 281} specified dynamics load ratings for the same}. Given that the service life of bearings in operation is significantly reduced, critical analysis is required to identify the possible failings of these standards. \cite{wes-7-387-2022}. White etching cracks are to be given focus in particular, as this phenomenon has been linked with premature aging of large bearings used in wind turbines.

The rotational symmetry of the bearing might lead to the expectation that rolling contact fatigue damage and failure-inducing cracks should be uniformly spread on the bearing raceway. However, inspection of failed bearings reveal axial cracks with regions of martensite. Martensite etches mildly with respect to the surrounding matrix and appear white when examined using optical microscopy, leading to them being named "white etching cracks". The root cause of white etching cracks has had various hypotheses have been put forward, with little consensus. A literature review of the phenomenon will be conducted, with emphasis on the most likely causations behind it.

The contact between the ball and bearing raceway will be analyzed with Hertzian contact models to find and analyze surface and subsurface stresses under varying load amplitudes. This will be used to estimate the depth of the axial cracks, with the intent of utilizing thermal spray coatings with high fracture toughness to prevent propagation of cracks.


\begin{figure}
  \includegraphics{white_etch_cracking.jpg}
  \caption[Example of white etch cracking]{Example of white etch cracking}
  \labfig{white_etch_cracking}
\end{figure}

%\subsection{White etching}

%In addition to IEC 61400-4 \sidenote{\href{https://www.iso.org/standard/44298.html}{IEC 61400} provides guidance on enclosed speed increasing gearboxes for horizontal axis wind turbine drivetrains}, wind turbine gear designs often follow the ISO 6336-5 standard \sidenote{\href{https://www.iso.org/standard/55353.html}{ISO 6336} specifies the load capacities of spur and helical gears.} \cite{wes-7-387-2022}. Gears are typically made from high Cr steel grades such as 4320, 4820, 9310, and 18CrNiMo7-6. These steel grades have low carbon and high chromium and molybdenum content, to increase the maximum toughness of the material. Wind turbine gearbox designs have been improving over the years resulting in the continuous revision of the IEC-61400 international standard, which outlines the minimum requirements for specification, design and verification of gearboxes in wind turbines [7]. Despite these continuous improvements, several technical challenges remain that have yet to be overcome, especially in the case of offshore wind farms.


%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

% The bibliography needs to be compiled with biber using your LaTeX editor, or on the command line with 'biber main' from the template directory

\printbibliography[title=References] % Set the title of the bibliography and print the references

\end{document}
